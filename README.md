# Code source des services de swordbot (Celeste)

Requière: Java 21


Pour lancer l'application :
```
./gradlew clean combat:bootRun
```
Le port d'écoute par défault est 8080 (Tomcat embarqué)

Pour plus de documentation voir [ici](https://gitlab.com/pacificator-consortium/majorjohn/sword/swordbot)

# To Do List :

# [X.X.X]
- [ ] Docker


# [0.0.1]
- [ ] Creer un nouveau module dédier au combat
- [ ] Définir le modèle (!= de la persistance)
- [ ] Définir les interfaces
- [ ] Service REST

