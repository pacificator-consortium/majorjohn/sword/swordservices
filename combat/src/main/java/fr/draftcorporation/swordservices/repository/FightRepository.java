package fr.draftcorporation.swordservices.repository;

import fr.draftcorporation.swordservices.model.Fight;
import org.springframework.data.jpa.repository.JpaRepository;
public interface FightRepository extends JpaRepository<Fight, Long> {
}
