package fr.draftcorporation.swordservices.controller;

import fr.draftcorporation.swordservices.model.Fight;
import fr.draftcorporation.swordservices.repository.FightRepository;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FightController {
    private final FightRepository repository;

    FightController(FightRepository repository) {
        this.repository = repository;
    }


    @GetMapping("/fights")
    List<Fight> all() {
        return repository.findAll();
    }

    @PostMapping("/fights")
    Fight newFight(@RequestBody Fight newFight) {
        return repository.save(newFight);
    }

    // Single item

    @GetMapping("/fights/{id}")
    Fight one(@PathVariable Long id) {

        return repository.findById(id)
                .orElseThrow(() -> new RuntimeException("" + id));
    }

    @PutMapping("/fights/{id}")
    Fight replaceFight(@RequestBody Fight newFight, @PathVariable Long id) {
        return repository.findById(id)
                .map(fight -> {
                    fight.setVersion(newFight.getVersion());
                    fight.setStartDate(newFight.getStartDate());
                    fight.setEndDate(newFight.getEndDate());
                    return repository.save(fight);
                })
                .orElseGet(() -> {
                    newFight.setId(id);
                    return repository.save(newFight);
                });
    }

    @DeleteMapping("/fights/{id}")
    void deleteFight(@PathVariable Long id) {
        repository.deleteById(id);
    }

}

