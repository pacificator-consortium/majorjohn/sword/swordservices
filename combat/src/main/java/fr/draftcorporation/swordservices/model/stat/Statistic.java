package fr.draftcorporation.swordservices.model.stat;

public abstract class Statistic {
    private long id;
    private int life;
    private int attack;
    private int defense;
    private int speed;
}
