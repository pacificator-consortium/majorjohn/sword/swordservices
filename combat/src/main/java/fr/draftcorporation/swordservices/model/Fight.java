package fr.draftcorporation.swordservices.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

import java.time.LocalDate;

@Entity
public class Fight {
    @Id
    @GeneratedValue
    private Long id;
    private String version;
    private LocalDate startDate;
    private LocalDate endDate;

//    private List<Player> playerList;
//    private List<Mob> mobList;
//    private FightTurn turn;


    public Fight() {
    }

    public Fight(String version, LocalDate startDate, LocalDate endDate) {
        this.version = version;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }
}
