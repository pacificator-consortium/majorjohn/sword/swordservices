package fr.draftcorporation.swordservices.model.fighter;

import fr.draftcorporation.swordservices.model.stat.InitialStatistic;
import fr.draftcorporation.swordservices.model.stat.NominalStatistic;

public abstract class Fighter {
    private long id;
    private String name;
    private InitialStatistic initialStatistic;
    private NominalStatistic nominalStatistic;
}
