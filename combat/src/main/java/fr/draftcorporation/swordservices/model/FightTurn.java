package fr.draftcorporation.swordservices.model;

import fr.draftcorporation.swordservices.model.fighter.Fighter;
import fr.draftcorporation.swordservices.model.stat.FightStatistic;

import java.util.Map;

public class FightTurn {
    private long id;
    private int turnIndex;
    private String textInput;
    private String nlpOutput;
    private Fight attackingFighter;
    private Map<Fighter, FightStatistic> fighterStatistics;
    private Map<Fighter, AttackResult> attackResults;

}
