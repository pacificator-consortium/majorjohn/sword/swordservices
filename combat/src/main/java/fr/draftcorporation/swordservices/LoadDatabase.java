package fr.draftcorporation.swordservices;

import fr.draftcorporation.swordservices.model.Fight;
import fr.draftcorporation.swordservices.repository.FightRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;

@Configuration
class LoadDatabase {

    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initDatabase(FightRepository repository) {

        return args -> {
            log.info("Preloading " + repository.save(new Fight("v1", LocalDate.now(), LocalDate.now())));
        };
    }
}